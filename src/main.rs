extern crate id3;

// use id3::Tag;

// enum Item {
    // Name(String),
    // Length(i32), //if using this for music, written, or visual media, length might refer to duration, page count... need to think
    // DateCreated(i32), // should use 1970 Epoch?
    // DateReleased(i32), // re-issue?
    // ID3(),
// }

fn main() {
  let tag = id3::Tag::read_from_path("otm010318_cms823946_pod.mp3").unwrap();

  // print the artist the hard way
  println!("{:?}", tag);

  // or print it the easy way
  println!("{}", tag.artist().unwrap());


 
  // use std::collections::HashMap;
  // let mut scores = HashMap::new();
  // scores.insert(String::from("Blue"), 10); scores.insert(String::from("Blue"), 25);
  // println!("{:?}", scores);
    
    

  // use std::collections::HashMap;
  // let field_name = String::from("Favorite color"); let field_value = String::from("Blue");
  // let mut map = HashMap::new(); map.insert(field_name, field_value);
  // println!("{}", field_name);
}
